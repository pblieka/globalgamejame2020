using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FirstPersonRaycaster : MonoBehaviour {
	#region Fields
	[SerializeField] private FirstPersonController __Controller;
	[SerializeField] private float __Distance;
	[SerializeField] private LayerMask __Layers;

	[SerializeField] private Transform __ReleasePosition;

	[SerializeField] private Material __TransparentHeldMaterial;
	[SerializeField] private Material __TransparentPlaceMaterial;
	[SerializeField] private Material __TransparentHoveredMaterial;
	private GameManager __Manager;
	private Piece __InventoryItem;
	private Piece __HoverItem;
	private InWorldButton __HoverButton;
	private Camera __Camera;
	// Audio - james
	private SoundControl __Sound;
	#endregion

	#region Initialization
	private void Awake() {
		__Manager = FindObjectOfType<GameManager>();
	}
	private void Start() {
		__Camera = Camera.main;
		__Sound = __Manager.music.GetComponent<SoundControl>();
	}
	#endregion

	#region Implementation
	private void Update() {
		Piece piece = null;
		Table table = null;
		InWorldButton button = null;
		RaycastHit hit;
		if(Physics.Raycast(__Camera.transform.position
			, __Camera.transform.forward
			, out hit
			, __Distance
			, __Layers.value)) {
			piece = hit.collider.GetComponentInParent<Piece>();
			table = hit.collider.GetComponentInParent<Table>();
			button = hit.collider.GetComponentInParent<InWorldButton>();
		}

		if(button != null) {
			if (__HoverButton != null) { __HoverButton.Hovered(false); }
			__HoverButton = button;
			__HoverButton.Hovered(true);
			if (Input.GetMouseButtonDown(0)) { button.DoAThing(); }
		} else if (__InventoryItem == null) {
			if (__HoverButton != null) { __HoverButton.Hovered(false); }
			__HoverButton = null;
			__HandleFreeHands(piece,table,button,hit);
		} else {
			if (__HoverButton != null) { __HoverButton.Hovered(false); }
			__HoverButton = null;
			__HandleHoldingSomething(piece,table,button,hit);
		}
	}
	private void __HandleHoldingSomething(Piece target, Table table, InWorldButton button, RaycastHit hit) {
		if (__Manager.State == GameManager.GameState.Running) { // only play while playing
			if (table != null || target != null
				&& target.AType == Piece.ActiveType.Static // avoid being able to place objects on nontethered objects
				&& __InventoryItem != null) {
				if (Input.GetMouseButtonDown(0)) {
					__PlaceObject(__InventoryItem, hit);
				}
				else { // render a preview
					Vector3 invCamFwd = -__Camera.transform.forward; // force object to face us, makes easier to place
					invCamFwd.y = 0f; invCamFwd.Normalize();
					__InventoryItem.transform.forward = invCamFwd;

					__InventoryItem.transform.up = hit.normal;
					__InventoryItem.transform.position = hit.point - hit.normal * __InventoryItem.Offset;
					__TransparentPlaceMaterial.mainTexture = __InventoryItem.Material.mainTexture;
					Graphics.DrawMesh(__InventoryItem.MeshFilter.sharedMesh
						, __InventoryItem.transform.localToWorldMatrix
						, __TransparentPlaceMaterial
						, 0);
				}
			}
			else { // throw object
				__InventoryItem.transform.position = __Camera.transform.position + __Camera.transform.forward * 1.5f;
				Vector3 invCamFwd = -__Camera.transform.forward; // force object to face us, makes easier to place
				invCamFwd.y = 0f; invCamFwd.Normalize();
				__InventoryItem.transform.forward = invCamFwd;

				__TransparentHeldMaterial.mainTexture = __InventoryItem.Material.mainTexture;
				Graphics.DrawMesh(__InventoryItem.MeshFilter.sharedMesh
					, __InventoryItem.transform.localToWorldMatrix
					, __TransparentHeldMaterial
					, 0);

				if (Input.GetMouseButtonDown(0)) {
					__Sound.PlayDetach();
					__InventoryItem.MakeActive(Piece.ActiveType.Physics);
					__InventoryItem.transform.position = __ReleasePosition.position;
					__InventoryItem.Body.AddForce(__Camera.transform.forward * 10f, ForceMode.Impulse);
					__InventoryItem = null;
				}
			}
		}
	}
	private void __HandleFreeHands(Piece target, Table table, InWorldButton button, RaycastHit hit) {
		if (__Manager.State == GameManager.GameState.Running) { // only play while playing
			if (target != null) {
				if (Input.GetMouseButtonDown(0)) {
					__Sound.PlayDetach();
					__InventoryItem = target;
					__InventoryItem.MakeActive(Piece.ActiveType.Off);
					__InventoryItem.transform.parent = null;

					// detach attached objects
					Piece[] attached = __InventoryItem.GetComponentsInChildren<Piece>();
					foreach (Piece p in attached) {
						if (p == __InventoryItem) { continue; }
						p.MakeActive(Piece.ActiveType.Physics);
						p.transform.parent = null;

						p.Body.AddForce((Vector3.up * 2f
							+ p.transform.position - __InventoryItem.transform.position).normalized * 5f, ForceMode.Impulse);
					}
				}
				else {
					if (__HoverItem != null) { __HoverItem.Hovered(null); }
					__HoverItem = target;
					__HoverItem.Hovered(__TransparentHoveredMaterial);
				}
			}
			else {
				if (__HoverItem != null) { __HoverItem.Hovered(null); }
				__HoverItem = null;
			}
		}
	}
	private void __PlaceObject(Piece target, RaycastHit hit) {
		__Sound.PlayAttach();
		target.MakeActive(Piece.ActiveType.Static);

		Vector3 invCamFwd = -__Camera.transform.forward; // force object to face us, makes easier to place
		invCamFwd.y = 0f; invCamFwd.Normalize();
		target.transform.forward = invCamFwd;

		target.transform.up = hit.normal;
		target.transform.position = hit.point - hit.normal * target.Offset;
		target.transform.parent = hit.transform;

		__InventoryItem = null;
	}
	#endregion

	#region Gizmos
	private void OnDrawGizmos() {
		if (Application.isPlaying) {
			Gizmos.DrawLine(__Camera.transform.position
				, __Camera.transform.position + __Camera.transform.forward * __Distance);
		}
	}
	#endregion
}
