using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour, ISerializationCallbackReceiver {
	#region Enum
	public enum ActiveType {
		Physics,
		Static,
		Off,
	}
	#endregion

	#region Fields
	public Rigidbody Body;
	public Renderer Renderer;
	public Material Material;
	public MeshFilter MeshFilter;
	public float Offset;
	public ActiveType AType;
	#endregion

	#region Initialization
	private void Awake() { }
	private void Start() { }
	#endregion

	#region Implementation
	public void MakeActive(ActiveType type) {
		AType = type;
		switch(type) {
			case ActiveType.Physics: 
				gameObject.SetActive(true);
				Body.isKinematic = false;
				break;
			case ActiveType.Static: 
				gameObject.SetActive(true);
				Body.isKinematic = true;
				break;
			case ActiveType.Off: 
				gameObject.SetActive(false);
				break;
		}
	}
	#endregion

	#region Selection
	public void Hovered(Material m) {
		if (m != null) {
			Renderer.sharedMaterial = m;
			m.mainTexture = Material.mainTexture;
		} else {
			Renderer.sharedMaterial = Material;
		}
	}
	#endregion

	#region OnDrawGizmosSelected
	public void OnDrawGizmosSelected() {
		Gizmos.color = Color.green;
		Gizmos.DrawLine(transform.position, transform.position + Vector3.up * Offset);
	}

	public void OnBeforeSerialize() {
		Body = GetComponentInChildren<Rigidbody>();
		Renderer = GetComponentInChildren<Renderer>();
		MeshFilter = GetComponentInChildren<MeshFilter>();
	}
	public void OnAfterDeserialize() { }
	#endregion
}
