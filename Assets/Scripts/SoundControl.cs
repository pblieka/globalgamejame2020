using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundControl : MonoBehaviour
{
	public AudioMixer Mixer;
	public AudioMixerSnapshot Fixed;
	public AudioMixerSnapshot Broken;
	public AudioMixerSnapshot Title;
	public AudioSource Explode;
	public AudioSource TitleClip;
	public AudioSource VoiceHello;
	public AudioSource Attach;
	public AudioSource Detach;
	public AudioSource Camera;
	public float FadeTime;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown("f"))
		{
			Broken.TransitionTo(FadeTime);
		}
		else if (Input.GetKeyDown(KeyCode.G))
		{
			Fixed.TransitionTo(FadeTime);
		}        
    }

	public void ExplodeSound()
	{
		Explode.Play();
	}

	public void TitleSound()
	{
		TitleClip.Play();
	}

	public void SayHello()
	{
		VoiceHello.Play();
	}

	public void PlayAttach()
	{
		Attach.Play();
	}

	public void PlayDetach()
	{
		Detach.Play();
	}

	public void PlayCamera()
	{
		Camera.Play();
	}
}
