using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {
	private Piece[] __Pieces;
	// Identifier for music
	public int CharacterTag;
	private void Awake() {
		__Pieces = GetComponentsInChildren<Piece>();
	}
	public void Explode() {
		for(int i = 0; i < __Pieces.Length; i += 1) {
			__Pieces[i].MakeActive(Piece.ActiveType.Physics);
			__Pieces[i].Body.AddForce((Vector3.up * 2f
				+ __Pieces[i].transform.position - transform.position).normalized * 5f, ForceMode.Impulse);
		}
	}
}
