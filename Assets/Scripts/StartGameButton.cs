using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameButton : InWorldButton {
	private GameManager __Manager;
	private void Start() {
		__Manager = FindObjectOfType<GameManager>();
	}
	public override void DoAThing() {
		switch(__Manager.State) {
			case GameManager.GameState.CharacterSelect: 
				__Manager.StartTheGame();
				break;
			case GameManager.GameState.Intro: 
				// do nothing
				break;
			case GameManager.GameState.Running: 
				__Manager.StopTheGame();
				break;
			case GameManager.GameState.Finish:
				__Manager.TryAgain();
				break;
			default:
				Debug.LogError("Unhandled State");
				break;
		}
	}
}
