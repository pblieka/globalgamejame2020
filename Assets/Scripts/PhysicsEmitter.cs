using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsEmitter : MonoBehaviour {
    [SerializeField] private ParticleSystem __SystemTemplate;
    [SerializeField] private AudioSource __SourceTemplate;

    [SerializeField] private float __ImpactThreshold;
	[SerializeField] private AudioClip[] __ImpactSounds;

    private ParticleSystem __System;
    private AudioSource __Source;

    void Awake()
    {
		if (__SystemTemplate != null) {
			__System = GameObject.Instantiate<ParticleSystem>(__SystemTemplate);
		}
		if (__SourceTemplate != null) {
			__Source = GameObject.Instantiate<AudioSource>(__SourceTemplate);
		}
    }

    void OnCollisionEnter(Collision c)
    {
        if(c.relativeVelocity.magnitude > __ImpactThreshold && c.contactCount > 0)
        {
            if(__System != null) {
				__System.Stop();
                __System.transform.position = c.GetContact(0).point;
                __System.transform.forward = c.GetContact(0).normal;
                __System.Play(true);
            }
            if (__Source != null)
            {
                float lt = 0;
                if(Time.realtimeSinceStartup - lt > 0.1f) { //don't play sounds too often
                    __Source.transform.position = c.GetContact(0).point;
					__Source.clip = __ImpactSounds[UnityEngine.Random.Range(0, __ImpactSounds.Length)];
                    __Source.Play();
                }
            }
        }
    }
}
