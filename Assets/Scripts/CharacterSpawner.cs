using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpawner : MonoBehaviour {
	public Character[] Characters;
	public Character ActiveCharacter;
	public void SpawnRandomCharacter() {
		GameObject.Destroy(ActiveCharacter);
		ActiveCharacter = GameObject.Instantiate<Character>(Characters[Random.Range(0,Characters.Length)]
			, transform.position, transform.rotation);
		ActiveCharacter.transform.parent = transform;
	}
}
