using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InWorldButton : MonoBehaviour {
	#region Fields
	public Renderer Renderer;
	public Material Material;
	#endregion

	#region Initialization
	private void Awake() { 
		Renderer = GetComponentInChildren<Renderer>();
		Material = Renderer.material;
	}
	#endregion

	#region Implementation
	public abstract void DoAThing();
	public void Hovered(bool value) {
		if (value) {
			Material.color = Color.green;
		}
		else {
			Material.color = Color.white;
		}
	}
	#endregion
}
