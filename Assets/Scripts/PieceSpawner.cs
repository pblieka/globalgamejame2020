using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceSpawner : MonoBehaviour {
	[SerializeField] private GameObject[] __Templates;
	[SerializeField] private float __Interval = 0.2f;
	private Coroutine __Routine;

	public void SpawnPieces(int number) {
		if(__Routine != null) { StopCoroutine(__Routine); }
		__Routine = StartCoroutine(__SpawnPieces(number));
	}
	private void Update() {
		if(Input.GetKeyDown(KeyCode.P)) {
			GameManager mgr = FindObjectOfType<GameManager>();
			if (mgr != null && mgr.State == GameManager.GameState.Running) {
				SpawnPieces(10);
			}
		}
	}
	public IEnumerator __SpawnPieces(int number) {
		for(int i = 0; i < number; i += 1) {
			GameObject g = GameObject.Instantiate<GameObject>(__Templates[Random.Range(0, __Templates.Length)], transform.position, transform.rotation);
			Piece p = g.GetComponentInChildren<Piece>();
			p.MakeActive(Piece.ActiveType.Physics);

			yield return new WaitForSeconds(__Interval);

			Vector2 randCirc = Random.insideUnitCircle.normalized;
			Vector3 randForce = new Vector3(randCirc.x, 2f, randCirc.y).normalized;
			p.Body.AddForce(randForce * 7f, ForceMode.Impulse);
		}
	}
}
