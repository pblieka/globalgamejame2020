using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour {
	[SerializeField] private string __LevelToLoad = "Game";
	[SerializeField] private PieceSpawner __PieceSpawner;
	[SerializeField] private Animator __UIAnimator;
	[SerializeField] private CharacterSpawner __CharacterSpawner;
	[SerializeField] private FirstPersonController __Controller;
	[SerializeField] private SoundControl __Sound;
	[SerializeField] private RawImage __ScreenShotPanel;

	public GameObject AudioPenguin;
	public GameObject AudioWalrus;
	public GameObject AudioNarwhal;
	public GameObject music;

	public enum GameState {
		FadeIn,
		CharacterSelect,
		Intro,
		Running,
		Finish
	}

	public GameState State = GameState.FadeIn;
	private Coroutine __FadeInCoroutine;
	private Coroutine __IntroCoroutine;

	private void Awake() {
		__CharacterSpawner = FindObjectOfType<CharacterSpawner>();

		__Controller.enabled = false;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		__FadeInCoroutine = StartCoroutine(DoFadeIn());
	}
	private void Update() {
		switch(State) {
			case GameState.CharacterSelect: break;
			case GameState.Intro: break;
			case GameState.Running: 
				if(Input.GetKeyDown(KeyCode.Return)) {
					StopTheGame();
				}
				break;
			case GameState.Finish: break;
		}
	}

	// Starting the game
	public IEnumerator DoFadeIn() {
		__CharacterSpawner.SpawnRandomCharacter();
		// Load music according to CharacterTag of spawned character
		switch (__CharacterSpawner.ActiveCharacter.CharacterTag)
		{
			case 1:
				// Penguin music
				music = Instantiate(AudioPenguin, new Vector3(0, 0, 0), Quaternion.identity);
				break;
			case 2:
				// Walrus music
				music = Instantiate(AudioWalrus, new Vector3(0, 0, 0), Quaternion.identity);
				break;
			case 3:
				// Narwhal music
				music = Instantiate(AudioNarwhal, new Vector3(0, 0, 0), Quaternion.identity);
				break;
			default: 
				// Penguin music
				music = Instantiate(AudioPenguin, new Vector3(0, 0, 0), Quaternion.identity);
				break;
		}
		__Sound = music.GetComponent<SoundControl>();
		__Sound.Title.TransitionTo(0.2f);
		__Sound.TitleSound();
		yield return new WaitForSeconds(6f);
		__Sound.SayHello();
		yield return new WaitForSeconds(3f);
		State = GameState.CharacterSelect;
		__Controller.enabled = true;
		StartTheGame();
	}
	public void StartTheGame() {
		State = GameState.Intro;
		__Sound.Broken.TransitionTo(__Sound.FadeTime);
		if(__IntroCoroutine != null) { StopCoroutine(__IntroCoroutine); }
		__IntroCoroutine = StartCoroutine(DoIntro());
	}
	public IEnumerator DoIntro() {
		// Playing explosion here - james
		__Sound.ExplodeSound();
		__CharacterSpawner.ActiveCharacter.Explode();
		yield return new WaitForSeconds(0.3f);
		State = GameState.Running;
		//__PieceSpawner.SpawnPieces(30);
		__IntroCoroutine = null;
	}
	public void StopTheGame() {
		__Controller.enabled = false;
		__Controller.GetComponent<FirstPersonRaycaster>().enabled = false;
		State = GameState.Finish;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		__Sound.Fixed.TransitionTo(__Sound.FadeTime);
		// show image and take a picture
		StartCoroutine(__RecordFrame());
	}
	private IEnumerator __RecordFrame() {
		yield return new WaitForEndOfFrame(); // suggested by unity
		__Sound.PlayCamera();
		var capture = ScreenCapture.CaptureScreenshotAsTexture();
		__ScreenShotPanel.texture = capture;
		__UIAnimator.SetTrigger("ShowEndGameScreen");
	}

	public void TryAgain() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
	}
	public void QuitGame() {
		Application.Quit();
	}
}
