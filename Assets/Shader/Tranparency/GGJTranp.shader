Shader "Unlit/GGJTranp" {
    Properties {
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,0)
		_Opacity ("Opacity", Range(0,1)) = 1
    }
    SubShader {
        Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }

        LOD 100
		Cull Back

		Pass {
			ZWrite On
			ColorMask 0
		}

        Pass {
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
				half4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
				float4 color : Color;
            };
            struct v2f {
				half4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
				half3 normal : NORMAL;
				half3 viewDir : TEXCOORD1;
				float4 color : Color;
            };

			// FIELDS
			sampler2D _MainTex;
			sampler2D _MainMaskTex;
            float4 _MainTex_ST;

			half4 _Color; 
			half _Opacity;

            v2f vert (appdata i) {
				v2f o;

				o.vertex = UnityObjectToClipPos(i.vertex);
				o.texcoord = i.texcoord;
				o.viewDir = normalize(mul(unity_ObjectToWorld, i.vertex).xyz - _WorldSpaceCameraPos);
				o.normal = UnityObjectToWorldNormal(i.normal);
				o.color = i.color;
				return o;
            }
            half4 frag (v2f i) : COLOR {
				// combine main and detail textures
				half4 tex = tex2D(_MainTex, i.texcoord);
				half d = sign(dot(i.viewDir, i.normal));
				tex.rgb = lerp(tex.rgb * _Color, half3(0,0,0), d*2) ;
				tex.a = _Opacity;
				return tex;
            }
            ENDCG
        }
    }
}
